<!-- cuando el usuario si esta logueado -->

<article class="container">
	<div class="row">
		<div class="col-md-2">
			<h4>Bienvenido <?php echo $_SESSION['conectado'];?></h4>
		</div>
		<div class="col-md-2 col-md-offset-8">
			<form action="index.php" method="post">
				<button type="submit" class="btn btn-danger btn-lg" value="Desconectar" name="desconectar"><span class="glyphicon glyphicon-user"></span> Desconectar</button>
			</form> 
		</div>
	</div>
</article>


