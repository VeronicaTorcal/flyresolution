
<!-- cuando el usuario no esta logueado -->

<article class="container">
	<div class="row">
		<div class="col-md-2 col-md-offset-8">
			<a href="login.html">
				<button type="submit" class="btn btn-success btn-lg" value="Nuevo usuario" name="nuevoUsuario"><span class="glyphicon glyphicon-user"></span> Hacer login</button>
			</a>
		</div>
		<div class="col-md-2">
			<a href="nuevo-usuario.html">
				<button type="submit" class="btn btn-primary btn-lg" value="Nuevo usuario" name="nuevoUsuario"><span class="glyphicon glyphicon-user"></span> Nuevo Usuario</button>
			</a>
		</div>
	</div>
</article>