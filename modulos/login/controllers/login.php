<?php  

//Incluyo mi modelo
require('modulos/login/models/loginModel.php');

//Comprobamos si se ha creado la variable de session
if(!isset($_SESSION['conectado'])){
	if(isset($_COOKIE['conectado'])){
		$_SESSION['conectado']=$_COOKIE['conectado'];
	}else{
		$_SESSION['conectado']=NULL;
	}
}

//Accion de Conectar
if(isset($_POST['conectar'])){
	$login=new LoginModel($_POST['nombreUsuario'], $_POST['password']);
	$_SESSION['conectado']=$login->comprobar(isset($_POST['recordar']));
}

//Accion de Desconectar
if(isset($_POST['desconectar'])){
	$_SESSION['conectado']=NULL;
	setcookie('conectado',null,0);
}

//Accion de comprobar si estamos o no conectados
if(isset($_SESSION['conectado'])){
	//Pinto la vista como que estoy conectado
	include('modulos/login/views/logoutView.php');
}else{
	//Pinto la vista como que estoy desconectado
	include('modulos/login/views/loginView.php');
}

?>