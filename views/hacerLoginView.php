
<!-- cuando el usuario no esta logueado -->


<article class="container">
	
	<div class="row">
		<div class="col-md-10 col-md-offset-1 fondo">
			<form action="index.php" method="post">
				<div class="input-login">
					<h4>Usuario</h4>
					<input type="text" name="nombreUsuario" class="form-control" value="Nombre Usuario" required >
				</div>
				<div class="input-login">
					<h4>Contraseña</h4>
					<input type="password" name="password" class="form-control" value="Contraseña"  required >
				</div>
				<div class="boton-login">
					<span>Recordar contraseña </span>
					<input type="checkbox" name="recordar">
					<button type="submit" class="btn btn-success btn-lg" value="Conectar" name="conectar"><span class="glyphicon glyphicon-user"></span> Conectar
					</button>
					
				</div>
			</form>
			
		</div>
		
	</div>
	
</article>