<article class="container fondo">
	
	<div class="row">

		<div class="col-md-12">
			<h4>3. Selecciona los tamaños a los que quieres redimensionar (Se creará una subcarpeta para cada tamaño de imagen).</h4>
		</div>
	</div>
	<form action="index.php?p=controlador.php&accion=paso3" method="post">
	<div class="row">
		<div class="col-md-6">
			<h4>Predefinidos</h4>
			<?php 
			foreach ($elementos as $resolucion) {
				echo '<input type="checkbox" value="'.$resolucion->dimeIdResolucion().'" name="resolucion[]">'.$resolucion->dimeResolucion().'<br>';
			}
			 ?>
			 <h6>Los formatos de imagen son cuadrados, se añadirá fondo blanco si tu foto es rectangular. Si quieres recortar la imagen haz click aquí:</h6>
			 <input type="checkbox" name="recortar"><b> Recortar</b><br>
			
		</div>
		<div class="col-md-6">
			<h4>Personalizado</h4>
			Ancho: <input type="text" name="ancho" placeholder="ancho en pixels">
			<br>
			<br>
			Alto: <input type="text" name="alto" placeholder="alto en pixels">
			<br>
			<br>
			Resolución: <input type="text" name="resolucion2" placeholder="p.p.p. de la imagen">
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-6">
			<h4>Extensiones</h4>
			<select name="idExtension" class="selectpicker">
			<?php 
			foreach ($elementosEx as $extension) {
				echo '<option value="'.$extension->dimeIdExtension().'">'.$extension->dimeExtension().'</option>';
			}
			 ?>
			 </select>
		</div>
	</div>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<button type="submit" class="col-md-12 btn btn-success btn-lg" value="ir al paso 3" name="enviar" >Ir al paso tres <span class="glyphicon glyphicon-hand-right"></span></button>
		</div>
	</div>
	</form>

			
	
</article>