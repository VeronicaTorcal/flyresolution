<?php 
	session_start();
	require_once('config.php');
	require_once('includes/conexion.php');
	require_once('models/MasterModel.php');
	//require_once('download.php');
	require_once('includes/functions.php');


	if (isset($_GET['p'])) {
			$p=$_GET['p'];
		}else{
			$p='controlador.php';
		}

 ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="css/estilos.css">
	<link href="uploadFiles/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
	<link href="uploadFiles/themes/explorer/theme.css" media="all" rel="stylesheet" type="text/css"/>
	 <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	 <script src="uploadFiles/js/plugins/sortable.js" type="text/javascript"></script>
    <script src="uploadFiles/js/fileinput.js" type="text/javascript"></script>
    <script src="uploadFiles/js/fileinput_locale_fr.js" type="text/javascript"></script>
    <script src="uploadFiles/js/fileinput_locale_es.js" type="text/javascript"></script>
    <script src="uploadFiles/themes/explorer/theme.js" type="text/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
	<link rel="icon" href="img/favicon.ico" type="image/x-icon">
	<title>FlyResolution</title>

</head>
<body>
	<header>
		<div class="row">
			<div class="container-fluid">
				<div class="col-md-5">
					<a href="index.php">
						<img src="img/mosca.png" alt="flyResolution" width="300" class="logo">
					</a>
				</div>
				<div class="col-md-7 titulo">
					<h1>FlyResolution.com</h1>
				</div>
			</div>
		</div>
		
	</header>
	<section class="container-fluid cuerpo">
		<!-- modulo del login -->
		<?php 

	
		include('modulos/login/controllers/login.php');
		

		 ?>
	</section>
	
	<section class="container-fluid cuerpo">
		<!-- contenido del controlador -->
		<?php 

		include('controllers/'.$p);

		 ?>
	</section>
	<footer><!-- PIE DE PAGINA --></footer>

</body>
<?php 

	//include('borrar.php');

	$directorio='imagenes/'; 
	$images=glob($directorio.'*.*');
 ?>
<script>

$("#archivos").fileinput({
    uploadUrl: "upload.php",
    uploadAsync: false,
    minFileCount: 1,
    maxFileCount: 100,
    showUpload: true,
    showRemove: true
});
    
</script>
</html>