<?php 

include('models/ImagenesModel.php');

$modelo=new ImagenesModel();


if (isset($_SESSION['conectado'])) {
	if (isset($_GET['accion'])) {
		$accion=$_GET['accion'];
	} else{
		$accion='paso1';
	}
}else{
	$accion='inicio';
}


switch ($accion) {
	case 'inicio':

		include('views/inicioView.php');
	
		break;

	case 'paso1':
		
		include('views/paso1View.php');
		break;

	case 'paso2':

		$elementos=$modelo->listadoResoluciones();
		$elementosEx=$modelo->listadoExtensiones();
		
		include('views/paso2View.php');
		break;

	case 'paso3':

		// recoge los id de resolucion y extension
		$res=0;
		if (isset($_POST['enviar'])) {
			foreach ($_POST['resolucion'] as $e) {
				$resolucion[$res]=$e;
				$res++;
			}
		}

		// recogemos si quieren recortar o no
		if (isset($_POST['recortar'])) {
		$recortar=1;
		} else {
			$recortar=0;
		}

		// la variable recortar ya tiene 1 o 0, se lo pasare a la funcion de redimensionar
	
		$extension=$_POST['idExtension'];
		//para saber cuantas imagenes hemos subido esta tanda, hacemos una variable de sesion
		$contador=$_SESSION['contador'];
		$modelo->insertarResolucionExtension($contador, $resolucion, $extension);
		
		// DEBE APARECER UN LISTADO DE LAS CARPETAS QUE SE VAN A CREAR.

		$carpetas=$modelo->crearNombreCarpeta($contador, $resolucion, $extension);
		
		$directorioNumero=$modelo->crearCarpetas($carpetas);
		//echo ' Esta es la extension: '.$extension;
		//echo 'las carpetas'.var_dump($carpetas);

		// en este momento habrá que meter las imagenes en cada carpeta creada, posteriormente redimensionarlas.
		
		$ruta='imagenes/';
		cambiarExtension($ruta, $extension);
		// YA SE HA CAMBIADO LA EXTENSION PERO SIGUEN EN LA CARPETA IMAGENES

		// HACER UNA FUNCION PASANDOLE $RUTA PARA COGER LAS IMAGENES Y $DIRECTORIONUMERO PARA DEJARLAS Y QUE LAS REDIMENSIONE Y LAS MUEVA ALLI
		copiarImagen($directorioNumero);
		// con esta funcion escaneo los directorios creados y elijo una funcion u otra para cambiarles el tamaño y meter las imagenes dentro
		// LO COMENTO DE MOMENTO
		escanearDirectorios($directorioNumero);

		//print_r($directoriosCreados);

		// hago una funcion diferente para cada tamaño

		//crearThumbnail($nombreImagen, $nombreThumbnail, $nuevoAncho, $nuevoAlto);

		// for ($i=0; $i < count($carpetas); $i++) { 

		// 	$rutaExcel=$directorioNumero.'/'.$carpetas[$i];
		// 	crearImagenExcel($directorioNumero, $rutaExcel);
			
		// }

		
		// UNA VEZ QUE YA ESTAN EN LA CARPETA QUE SE VA A DESCARGAR, REDIMENSIONAR Y MOVER A CADA CARPETA QUE SE HAYA CREADO
		// voy a crear una funcion diferente para cada dimension, llamo a una u otra segun las carpetas que se hayan creado
		

		include('views/paso3View.php');
		break;

	case 'descargar':

		// AQUI HABRA QUE ANCLAR LA FUNCION QUE DESCARGA EL ZIP

		$ruta=$_POST['directorioNumero'];
		// $ruta.='/';
		//$ruta="imagenes/";
		$zip_salida='suputamadre.zip';
		if(isset($_POST['descargar'])){
			echo $ruta;
   			comprimir($ruta, $zip_salida, $handle = false, $recursivo = false);
   			descargar($zip_salida);
   			borrarImagenes();
			
			// header('Location:index.php?p=controlador.php&accion=hecho');
		}

		// AQUI FALLA, SI LE DIGO LA RUTA EXPLICITAMENTE LA DESCARGA BIEN, PERO SI SE LA PASO POR POST DICE QUE EL DIRECTORIO ESTA VACIO

	case 'hecho':
		
		include('views/hechoView.php');

		break;
	

	case 'volverIntroducirPassword':

		$numRecuperar=$_POST['numRecuperar'];
		// llamar a funcion que dice el id sorteo, introduciendo el codigo
		$elementos=$modeloA->recuperarSorteo($numRecuperar);

		if ($elementos!=false) {
			
			include('views/volverEnviarView.php');

		} else {
			include('views/numeroNoExisteView.php');
		}
		// UN FOR IGUAL QUE EL DE INSERTAR CON LOS CORREOS DE LOS AMIGOS, LOS NOMBRES DEJARLOS IGUAL
		break;

	case 'modificacionPassword':

		if (isset($_POST['idAmigo'])) {
			// este campo le llega por hidden
			$idSorteo=$_POST['idSorteo'];
			$idAmigo=$_POST['idAmigo'];
			$correoAmigo=$_POST['correoAmigo'];
		}
		
		$modeloA->modificacionCorreo($idAmigo, $correoAmigo);
		$modeloCorreo=new EnviarCorreoModel();
		$modeloCorreo->enviarCorreo($idSorteo, $idAmigo);
		include('views/emailEnviadoView.php');
		break;
	
}

 ?>

