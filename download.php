<?php 

function comprimir($ruta, $zip_salida, $handle = false, $recursivo = false){
 
  if(!is_dir($ruta) and !is_file($ruta))
   return false; /* La ruta no existe */
 
  /* Declara el handle del objeto */
  if(!$handle){
   $handle = new ZipArchive;
   if ($handle->open($zip_salida, ZipArchive::CREATE) === false){
    return false; /* Imposible crear el archivo ZIP */
   }
  }
 
  /* Procesa directorio */
  if(is_dir($ruta)){
   $ruta = dirname($ruta.'/arch.ext'); /* Aseguramos que sea un directorio sin carácteres corruptos */
   $handle->addEmptyDir($ruta); /* Agrega el directorio comprimido */
   foreach(glob($ruta.'/*') as $url){ /* Procesa cada directorio o archivo dentro de el */
    comprimir($url, $zip_salida, $handle, true); /* Comprime el subdirectorio o archivo */
   }
 
  /* Procesa archivo */
  }else
   $handle->addFile($ruta);
 
  /* Finaliza el ZIP si no se está ejecutando una acción recursiva en progreso */
  if(!$recursivo)
   $handle->close();
 
  return true; /* Retorno satisfactorio */
 }

 function descargar($zip_salida){

 	header('Content-Description: File Transfer');
	header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename='.basename($zip_salida));
	header('Content-Transfer-Encoding: binary');
	header('Expires: 0');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');
	header('Content-Length: ' . filesize($zip_salida));
	ob_clean();
	flush();
	readfile($zip_salida);
  unlink($zip_salida);
	
 }

// lo de esta funcion no va asi, si ese codigo lo meto en la funcion de arriba si que funciona
 function borrarImagenes(){

  $files = glob('imagenes/*'); // obtiene todos los archivos
  foreach($files as $file){
    if(is_file($file)) // si se trata de un archivo
      unlink($file); // lo elimina
  }
 }

 ?>