<?php 

function comprimir($ruta, $zip_salida, $handle = false, $recursivo = false){
 
  if(!is_dir($ruta) and !is_file($ruta))
   return false; /* La ruta no existe */
 
  /* Declara el handle del objeto */
  if(!$handle){
   $handle = new ZipArchive;
   if ($handle->open($zip_salida, ZipArchive::CREATE) === false){
    return false; /* Imposible crear el archivo ZIP */
   }
  }
 
  /* Procesa directorio */
  if(is_dir($ruta)){
   $ruta = dirname($ruta.'/arch.ext'); /* Aseguramos que sea un directorio sin carácteres corruptos */
   $handle->addEmptyDir($ruta); /* Agrega el directorio comprimido */
   foreach(glob($ruta.'/*') as $url){ /* Procesa cada directorio o archivo dentro de el */
    comprimir($url, $zip_salida, $handle, true); /* Comprime el subdirectorio o archivo */
   }
 
  /* Procesa archivo */
  }else
   $handle->addFile($ruta);
 
  /* Finaliza el ZIP si no se está ejecutando una acción recursiva en progreso */
  if(!$recursivo)
   $handle->close();
 
  return true; /* Retorno satisfactorio */
 }

 function descargar($zip_salida){

 	header('Content-Description: File Transfer');
	header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename='.basename($zip_salida));
	header('Content-Transfer-Encoding: binary');
	header('Expires: 0');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');
	header('Content-Length: ' . filesize($zip_salida));
	ob_clean();
	flush();
	readfile($zip_salida);
  unlink($zip_salida);
	
 }

 function borrarImagenes(){

  $files = glob('imagenes/*'); // obtiene todos los archivos
  foreach($files as $file){
    if(is_file($file)) // si se trata de un archivo
      unlink($file); // lo elimina
  }
 }


  function cambiarExtension($ruta, $extension){
  // abrir carpeta imagenes
  $manejador=opendir($ruta);
  while (false !== ($archivo = readdir($manejador))) {
    if ($archivo != "." && $archivo != "..") {
      $cadena=$ruta.$archivo;
      // partimos el archivo cuando llegue al punto, para que sustituya la extension
      $partirCadena=explode(".", $cadena);
      $imagen='';

      switch ($partirCadena[1]) {
        case 'jpg'||'jpeg':
        // abrimos imagen jpg
          $imagen=imagecreatefromjpeg($cadena);
          break;
        case 'png':
        // abrimos imagen jpg
          $imagen=imagecreatefrompng($cadena);
          break;
        case 'gif':
        // abrimos imagen jpg
          $imagen=imagecreatefromgif($cadena);
          break;
        case 'bmp':
        // no se porque no sale como funcion
          $imagen=imagecreatefrombmp($cadena);
          break;
        case 'wbmp':
        // abrimos imagen jpg
          $imagen=imagecreatefromwbmp($cadena);
          break;
        
        default:
          # code...
          break;
      }

      switch ($extension) {
        case '1':
          $imagenNueva=$partirCadena[0].'.jpg';
          imagejpeg($imagen, $imagenNueva);
          unlink($cadena);
          //rename($cadena, $partirCadena[0].".jpg");
          break;
        case '2':
          $imagenNueva=$partirCadena[0].'.gif';
          imagegif($imagen, $imagenNueva);
          unlink($cadena);
          // rename($cadena, $partirCadena[0].".raw");
          break;
        case '3':
          $imagenNueva=$partirCadena[0].'.bmp';
          imagebmp($imagen, $imagenNueva);
          unlink($cadena);
          // rename($cadena, $partirCadena[0].".tiff");
          break;
        case '4':
          $imagenNueva=$partirCadena[0].'.wbmp';
          imagewbmp($imagen, $imagenNueva);
          unlink($cadena);
          // rename($cadena, $partirCadena[0].".eps");
          break;
        case '5':
          $imagenNueva=$partirCadena[0].'.png';
          imagepng($imagen, $imagenNueva);
          //imagedestroy($imagen);
          unlink($cadena);
          break;
        
        default:
          echo 'no se puede cambiar la extension';
          break;
      }
      
    }
  }
  closedir($manejador);
 }

 function copiarImagen($destino){

 	$origen='imagenes/';
 	$destino.='/';
 	$manejador=opendir($origen);
 	while (false !== ($archivo = readdir($manejador))) {
 		$origen='imagenes/';
 		if ($archivo != "." && $archivo != "..") {
 					
	 			copy($origen.$archivo, $destino.$archivo);	
 		}
 	}
  
 }


 // function crearImagenExcel($directorioNumero, $rutaExcel, $ancho=250, $alto=250, $pixels=72){

 //  // FALLOS: COMO YA SE HA CAMBIADO LA EXTESION DE LA IMAGEN, CREO QUE CON UN SWITCH SERIA SUFICIENTE. FALTA HACER UN OPENDIR DE LA RUTA EXCEL
 //  // NO FUNCIONA PORQUE PRIMERO TIENE QUE ESTAR IMAGECREATEFROM TAL, Y LUEGO IMAGECREATETRUECOLOR

 //    $directorioNumero.='/';
 //    $manejador=opendir($directorioNumero);
 //    while (false !== ($archivo = readdir($manejador))) {
 //      if ($archivo != '.' && $archivo != '..') {
 //        $extension=explode(".", $archivo);
 //        $manejadorDos=opendir($rutaExcel);

 //      if ($imagen !== false) {
        
 //        $excel=imageCreatetrueColor($ancho, $alto);
 //        if ($excel !== false) {
 //          $anchoThumb=imagesx($imagen);
 //          $altoThumb=imagesy($imagen);
 //          imagecopyresampled($excel, $imagen, 0, 0, 0, 0, $ancho, $alto, $anchoThumb, $altoThumb);
 //          switch ($extension) {
 //            case 'jpg'||'jpeg':
 //            // creamos imagen jpg
 //              $imagen=imagecreatefromjpeg($directorioNumero.$archivo);
 //              $resultado = imagejpeg($excel,$rutaExcel,$calidad);
 //              break;
 //            case 'png':
 //              $imagen=imagecreatefrompng($directorioNumero.$archivo);
 //              $resultado = imagepng($excel,$rutaExcel,$calidad);
 //              break;
 //            case 'gif':
 //              $imagen=imagecreatefromgif($directorioNumero.$archivo);
 //              $resultado = imagegif($excel,$rutaExcel,$calidad);
 //              break;
 //            case 'bmp':
 //              $imagen=imagecreatefrombmp($directorioNumero.$archivo);
 //            // no se porque no sale como funcion
 //              $resultado = imagebmp($excel,$rutaExcel,$calidad);
 //              break;
 //            case 'wbmp':
 //            $imagen=imagecreatefromwbmp($directorioNumero.$archivo);
 //              $resultado = imagewbmp($excel,$rutaExcel,$calidad);
 //              break;
            
 //            default:
 //              # code...
 //            break;
 //         } //fin del switch
 //        } //fin del if
 //      }//fin del if
   
 //      }//fin del if
 //    }//fin del while

 // }

function escanearDirectorios($directorioNumero){

    $directoriosCreados=scandir($directorioNumero);
    $imagenes='';
    // se crea array asociativo que incluye ".", "..", carpetas y archivos
    //echo $directorioNumero;
    foreach ($directoriosCreados as $e) {
      if ($e != "." && $e != "..") {
        // if (file($directorioNumero.'/'.$e)) {
        //   $imagenes+=$imagenes[$e];
        //   echo $imagenes;
        // }

        if (is_dir($directorioNumero.'/'.$e)==true) {
          // AQUI ENTRA SOLO PARA LAS CARPETAS, PASANDO DE LOS archivos
          // HABRÍA QUE RECONOCER CUAL ES CUAL, PARA METERLE LAS FOTOS REDIMENSIONADAS
          if (substr_count($e, 'imprenta') == 1){
            // LLAMAR A FUNCION PARA METER LAS FOTOS DE IMPRENTA Y PASARLE LA RUTA Y LAS IMAGENES
            echo 'hay imagen para imprenta';
          }
          if (substr_count($e, 'web') == 1){
            echo 'hay imagen para web';
          }
          if (substr_count($e, 'excel') == 1){
            echo 'hay imagen para excel';
            // habría que pasarle:array de nombreImagen con ruta, nombre de imagen creada con ruta, nuevo alto y nuevo ancho 
            crearImagenExcel($e);
          }
          
        }
      }
    }
 }

 function crearImagenExcel($carpetaExcel){
  echo 'Entra en la funcion, nombre del excel:<br> '.$carpetaExcel;
 }


/**
 * Crea un thumbail de una imagen con el ancho y el alto pasados como parámetros. Si se pone el
 * ancho o el alto a 0, este se calculara para que la imagen conserve las proporciones originales.
 * 
 * @param type $nombreImagen Nombre completo de la imagen incluida la ruta y la extension.
 * @param type $nombreThumbnail Nombre completo para el thumbnail incluida la ruta y la extension.
 * @param type $nuevoAncho Ancho para el thumbnail.
 * @param type $nuevoAlto Alto para el thumbnail.
 */
// function crearThumbnail($nombreImagen, $nombreThumbnail, $nuevoAncho, $nuevoAlto){
    
//     // Obtiene las dimensiones de la imagen.
//     list($ancho, $alto) = getimagesize($nombreImagen);
 
//     // Establece el alto para el thumbnail si solo se paso el ancho.
//     if ($nuevoAlto == 0 && $nuevoAncho != 0){
//         $factorReduccion = $ancho / $nuevoAncho;
//         $nuevoAlto = $alto / $factorReduccion;
//     }
    
//     // Establece el ancho para el thumbnail si solo se paso el alto.
//     if ($nuevoAlto != 0 && $nuevoAncho == 0){
//         $factorReduccion = $alto / $nuevoAlto;
//         $nuevoAncho = $ancho / $factorReduccion;
//     }
             
//     // Abre la imagen original.
//     list($imagen, $tipo) = abrirImagen($nombreImagen);
    
//     // Crea la nueva imagen (el thumbnail).
//     $thumbnail = imagecreatetruecolor($nuevoAncho, $nuevoAlto);  
//     imagecopyresampled($thumbnail , $imagen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
    
//     // Guarda la imagen.
//     guardarImagen($thumbnail, $nombreThumbnail, $tipo);
// }



 ?>