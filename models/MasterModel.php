<?php  


Class MasterModel {

	protected $conexion;
	protected $tabla;
	protected $campoId;
	// esa variable la hago equivalente a la campoOrden del otro ejercicio. Se supone que es para guardar el primer campo de cada tabla, es decir, el id


	public function __construct($tabla){

		$this->conexion=Conexion::conectar();
		$this->tabla=$tabla;

		$sql="SELECT * FROM $tabla LIMIT 0,1";

		 $consulta=$this->conexion->query($sql);
		 $this->campoId=$consulta->fetch_fields()[0]->name;

	}		
}

?>