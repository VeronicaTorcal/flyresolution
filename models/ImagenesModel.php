<?php 

include('models/ImagenModel.php');
include('models/ExtensionModel.php');
include('models/ResolucionModel.php');


	Class ImagenesModel extends MasterModel{ 



		public function __construct(){

			parent::__construct('imagenes');
			$this->campoOrden='idImagen';
		}

		public function listadoResoluciones(){
			$sql="SELECT * FROM resoluciones ORDER BY idResolucion";
			$consulta=$this->conexion->query($sql);
			while($fila=$consulta->fetch_array()){
				$res=new ResolucionModel($fila['idResolucion'], $fila['resolucion'], $fila['pppResolucion']);
				$this->elementos[]=$res;
			}
			return $this->elementos;

		}

		public function listadoExtensiones(){
			$sql="SELECT * FROM extensiones ORDER BY idExtension";
			$consulta=$this->conexion->query($sql);
			while($fila=$consulta->fetch_array()){
				$ext=new ExtensionModel($fila['idExtension'], $fila['extension']);
				$this->elementosEx[]=$ext;
			}
			return $this->elementosEx;

		}

		public function insertarResolucionExtension($contador, $resolucion, $extension){

			// SELECCIONO LAS ULTIMAS IMAGENES INTRODUCIDAS
			$sql="SELECT * FROM $this->tabla ORDER BY idImagen DESC LIMIT ".$contador;
			$consulta=$this->conexion->query($sql);

			// POR CADA RESULTADO MODIFICAMOS LA TABLA CON LA RESOLUCION ELEGIDA
			while ($fila=$consulta->fetch_array()) {
				
				$sqlE="UPDATE imagenes SET idExtension='$extension' WHERE idImagen=".$fila[0];
				$consultaE=$this->conexion->query($sqlE);
			// INSERTAR EN RESOLUCIONESIMAGENES TODOS LOS INSERTADOS EN EL CHECKBOX
			for ($i=0; $i < sizeof($resolucion); $i++) { 
				$sqlR="INSERT INTO resolucionesimagenes (idImagen, IdResolucion) VALUES ($fila[0], $resolucion[$i])";
				$consultaR=$this->conexion->query($sqlR);
			}
			}

		}

		public function crearNombreCarpeta($contador, $resolucion, $extension){

			// HACE FALTA LA FECHA PARA CREAR EL NOMBRE DE LAS CARPETAS

			$fecha=date('Y-m-d');
			$nombreCarpeta='';

			// SACAMOS EL NOMBRE DE LA EXTENSION CON SU ID

			$sql="SELECT extension FROM extensiones WHERE idExtension=$extension";
			$consulta=$this->conexion->query($sql);
			$resultado=$consulta->fetch_row();

			$nombreExtension=$resultado[0];

			// POR CADA RESOLUCION LE ADJUDICAMOS UN STRING PARA EL NOMBRE DE LAS CARPETAS

			for ($i=0; $i < sizeof($resolucion); $i++) { 

				switch ($resolucion[$i]) {
					case '1':
						$nombreResolucion='imprenta';
						break;
					case '2':
						$nombreResolucion='excel';
						break;
					case '3':
						$nombreResolucion='web';
						break;
					
					default:
						$nombreResolucion='personalizado';
						break;
				}
			
				$nombreCarpeta=$fecha.'_'.$nombreResolucion.'_'.$nombreExtension;
				
				$this->carpetas[]=$nombreCarpeta;
			}
			return $this->carpetas;
		}


		public function crearCarpetas($carpetas){

			$ruta='descargas/';
			//$hayFecha=scandir($ruta);
			$fecha=date('Y-m-d');
			$contadorCarpetas=1;
			$rutaFecha=$ruta.$fecha;

			if (is_dir($rutaFecha)) {
				// la carpeta con fecha de hoy ya esta creada
				$carpeta = @scandir($rutaFecha);
				if (count($carpeta) > 2) {
					// ya hay fecha de hoy, y hay cosas dentro
					echo 'en la carpeta hay cosas';
					//var_dump($carpeta);
					for ($i=0; $i < count($carpeta); $i++) { 
						$contadorCarpetas++;
					}
					$contadorCarpetas=$contadorCarpetas-2;
					// creamos nueva carpeta de numero
					$carpetaNumero=htmlspecialchars($contadorCarpetas);
					$directorioNumero=$rutaFecha.'/'.$contadorCarpetas;
					if (!is_dir($directorioNumero)) {
					$crear=mkdir($directorioNumero, 0777, true);
						if ($crear) {
							$mensaje="Directorio de primer numero creado correctamente";
					
							for ($i=0; $i < sizeof($carpetas); $i++){
								$subcarpeta=htmlspecialchars($carpetas[$i]);
								$directorioSubcarpeta=$directorioNumero.'/'.$subcarpeta;
								if (!is_dir($directorioSubcarpeta)) {

									$crear=mkdir($directorioSubcarpeta, 0777, true);
									if ($crear) {
										$mensaje='Directorio de resolucion creado correctamente';
									} else {
										$mensaje="Ha ocurrido un error al crear directorio de resolucion";
									}
								} else {

									$mensaje="El directorio que intentas crear ya existe";
								}

							}
						} else {
							$mensaje="Ha ocurrido un error al crear directorio de primer numero";
						}
					}

				} else {
					// ya hay carpeta de hoy pero no hay nada aun
					echo 'en la carpeta no hay nada';
					$carpetaNumero=htmlspecialchars($contadorCarpetas);
					$directorioNumero=$rutaFecha.'/'.$contadorCarpetas;
					if (!is_dir($directorioNumero)) {
					$crear=mkdir($directorioNumero, 0777, true);
						if ($crear) {
							$mensaje="Directorio de primer numero creado correctamente";
					
							for ($i=0; $i < sizeof($carpetas); $i++){
								$subcarpeta=htmlspecialchars($carpetas[$i]);
								$directorioSubcarpeta=$directorioNumero.'/'.$subcarpeta;
								if (!is_dir($directorioSubcarpeta)) {

									$crear=mkdir($directorioSubcarpeta, 0777, true);
									if ($crear) {
										$mensaje='Directorio de resolucion creado correctamente';
									} else {
										$mensaje="Ha ocurrido un error al crear directorio de resolucion";
									}
								} else {

									$mensaje="El directorio que intentas crear ya existe";
								}

							}
						} else {
							$mensaje="Ha ocurrido un error al crear directorio de primer numero";
						}
					}
				}

			} else {
				echo 'no existe carpeta con fecha de hoy';
				// creamos la carpeta del dia de hoy
				$carpetaFecha=htmlspecialchars($fecha);
				$directorioFecha=$ruta.$carpetaFecha;
				if (!is_dir($directorioFecha)) {
					$crear=mkdir($directorioFecha, 0777, true);
					if ($crear) {
						$mensaje="Directorio de fecha creado correctamente";
					} else {
						$mensaje="Ha ocurrido un error al crear directorio de fecha";
					}
				}

				$carpetaNumero=htmlspecialchars($contadorCarpetas);
					$directorioNumero=$rutaFecha.'/'.$contadorCarpetas;
					if (!is_dir($directorioNumero)) {
					$crear=mkdir($directorioNumero, 0777, true);
						if ($crear) {
							$mensaje="Directorio de primer numero creado correctamente";
							for ($i=0; $i < sizeof($carpetas); $i++){
								$subcarpeta=htmlspecialchars($carpetas[$i]);
								$directorioSubcarpeta=$directorioNumero.'/'.$subcarpeta;
								if (!is_dir($directorioSubcarpeta)) {

									$crear=mkdir($directorioSubcarpeta, 0777, true);
									if ($crear) {
										$mensaje='Directorio de resolucion creado correctamente';
									} else {
										$mensaje="Ha ocurrido un error al crear directorio de resolucion";
									}
								} else {

									$mensaje="El directorio que intentas crear ya existe";
								}

							}
						} else {
							$mensaje="Ha ocurrido un error al crear directorio de primer numero";
						}
					}


			}

			// ya hay carpeta de fecha de hoy si o si
			return $directorioNumero;

		}


	
		
	}

 ?>