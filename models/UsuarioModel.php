<?php 

	Class UsuarioModel{

		private $idUsuario;
		private $nombreUsuario;
		private $correoUsuario;
		private $password;

		public function __construct($idUsuario, $nombreUsuario, $correoUsuario, $password){

			$this->idUsuario=$idUsuario;
			$this->nombreUsuario=$nombreUsuario;
			$this->correoUsuario=$correoUsuario;
			$this->password=$password;
	
		}

		public function dimeIdUsuario(){
		return $this->idUsuario;
		}

		public function dimeNombreUsuario(){
			return $this->nombreUsuario;
		}
		
		public function dimeCorreoUsuario(){
			return $this->correoUsuario;
		}

		public function dimePassword(){
			return $this->password;
		}

	
	}

 ?>