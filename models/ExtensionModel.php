<?php 

	Class ExtensionModel{

		private $idExtension;
		private $extension;

		public function __construct($idExtension, $extension){

			$this->idExtension=$idExtension;
			$this->extension=$extension;
	
		}

		public function dimeIdExtension(){
			return $this->idExtension;
		}

		public function dimeExtension(){
			return $this->extension;
		}

	
	}

 ?>