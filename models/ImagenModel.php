<?php 

	Class ImagenModel{

		private $idImagen;
		private $nombreImagen;
		private $idExtension;

		public function __construct($idImagen, $nombreImagen, $idExtension){

			$this->idImagen=$idImagen;
			$this->nombreImagen=$nombreImagen;
			$this->idExtension=$idExtension;
	
		}

		public function dimeIdImagen(){
		return $this->idExtension;
		}

		public function dimeNombreImagen(){
			return $this->nombreImagen;
		}
		
		public function dimeExtensionImagen(){
			return $this->idExtension;
		}

	
	}

 ?>