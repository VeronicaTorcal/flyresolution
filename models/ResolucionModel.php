<?php 

	Class ResolucionModel{

		private $idResolucion;
		private $resolucion;
		private $pppResolucion;

		public function __construct($idResolucion, $resolucion, $pppResolucion){

			$this->idResolucion=$idResolucion;
			$this->resolucion=$resolucion;
			$this->pppResolucion=$pppResolucion;
	
		}

		public function dimeIdResolucion(){
			return $this->idResolucion;
		}

		public function dimeResolucion(){
			return $this->resolucion;
		}

		public function dimePPP(){
			return $this->pppResolucion;
		}

	
	}

 ?>